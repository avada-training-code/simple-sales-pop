import React, {useState} from 'react';
import {
  Layout,
  Page,
  LegacyCard,
  ResourceList,
  ResourceItem,
  Text,
  Pagination
} from '@shopify/polaris';
import NotificationPopup from '../../components/NotificationPopup/NotificationPopup';
import useFetchApi from '../../hooks/api/useFetchApi';
import NotificationEmpty from './NotificationEmpty';

/**
 * Just render a sample page
 *
 * @return {React.ReactElement}
 * @constructor
 */
export default function Notifications() {
  const [sortValue, setSortValue] = useState('DATE_MODIFIED_DESC');
  const [selectedItems, setSelectedItems] = useState([]);
  const resourceName = {
    singular: 'customer',
    plural: 'customers'
  };
  const promotedBulkActions = [
    {
      content: 'Edit customers',
      onAction: () => console.log('Todo: implement bulk edit')
    }
  ];
  const items = [
    {
      id: '106',
      url: '#',
      name: 'Mae Jemison',
      location: 'Decatur, USA'
    }
  ];

  const {loading, data: input} = useFetchApi({
    url: '/notifications',
    defaultData: items
  });

  function renderItem(item) {
    const {id} = item;

    return (
      <ResourceItem id={id}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between'
          }}
        >
          <NotificationPopup
            firstName={item.firstName}
            city={item.city}
            country={item.country}
            productName={item.productName}
            timestamp="a day ago"
            productImage={item.productImage}
          />
          <div style={{width: '92px'}}>
            <Text as="p" variant="bodyLg">
              From March 8, 2021
            </Text>
          </div>
        </div>
      </ResourceItem>
    );
  }
  function resolveItemIds(id) {
    return id;
  }
  return (
    <div>
      {loading && !input.shopId ? (
        <NotificationEmpty />
      ) : (
        <Page fullWidth title="Notifications" subtitle="List of sales notifcation from Shopify">
          <Layout>
            <Layout.Section>
              <LegacyCard>
                <ResourceList
                  resourceName={resourceName}
                  items={input}
                  renderItem={renderItem}
                  sortValue={sortValue}
                  sortOptions={[
                    {label: 'Newest update', value: 'DATE_MODIFIED_DESC'},
                    {label: 'Oldest update', value: 'DATE_MODIFIED_ASC'}
                  ]}
                  onSortChange={selected => {
                    setSortValue(selected);
                    console.log(`Sort option changed to ${selected}.`);
                  }}
                  selectedItems={selectedItems}
                  onSelectionChange={setSelectedItems}
                  promotedBulkActions={promotedBulkActions}
                  resolveItemId={resolveItemIds}
                />
              </LegacyCard>{' '}
              <div
                style={{
                  width: '100%',
                  display: 'flex',
                  justifyContent: 'center',
                  marginTop: '20px'
                }}
              >
                <Pagination
                  hasPrevious
                  onPrevious={() => {
                    console.log('Previous');
                  }}
                  hasNext
                  onNext={() => {
                    console.log('Next');
                  }}
                />
              </div>
            </Layout.Section>
          </Layout>
        </Page>
      )}
    </div>
  );
}
