import React, {useEffect, useState} from 'react';
import {Layout, Page, SettingToggle, Text} from '@shopify/polaris';
import '../../styles/layout/home.scss';
import {api} from '../../helpers';
/**
 * Render a home page for overview
 *
 * @return {React.ReactElement}
 * @constructor
 */
export default function Home() {
  const [enabled, setEnabled] = useState(false);

  const fetchData = async () => {
    try {
      const res = await api('/settings');
      console.log(res);
    } catch (error) {}
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Page title="Home" fullWidth>
      <Layout>
        <Layout.Section>
          <SettingToggle
            action={{
              content: enabled ? 'Disable' : 'Enable',
              onAction() {
                setEnabled(prev => !prev);
              }
            }}
            enabled={enabled}
          >
            <Text variant="bodyMd" as="span">
              App status is{' '}
              {enabled ? (
                <span className="Avada-Home__status-text">enabled</span>
              ) : (
                <span className="Avada-Home__status-text">disabled</span>
              )}
            </Text>
          </SettingToggle>
        </Layout.Section>
      </Layout>
    </Page>
  );
}
