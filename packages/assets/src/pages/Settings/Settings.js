import React, {useCallback, useState} from 'react';
import {
  Button,
  Checkbox,
  Grid,
  Layout,
  LegacyCard,
  LegacyTabs,
  Page,
  RangeSlider,
  TextField
} from '@shopify/polaris';
import NotificationPopup from '../../components/NotificationPopup/NotificationPopup';
import './Settings.scss';
import Triggers from './Triggers';
import DesktopPosition from '../../components/DesktopPosition/DesktopPosition';
import useFetchApi from '../../hooks/api/useFetchApi';
import defaultSetting from '../../const/settings/defaultSettings';
import SettingEmpty from './SettingEmpty';
import useEditApi from '../../hooks/api/useEditApi';

/**
 * @return {JSX.Element}
 */
export default function Settings() {
  const [selected, setSelected] = useState(0);

  const handleTabChange = useCallback(selectedTabIndex => setSelected(selectedTabIndex), []);

  const {loading, data: input, setData: setInput} = useFetchApi({
    url: '/settings',
    defaultData: defaultSetting
  });
  const {editing, handleEdit} = useEditApi({
    url: '/settings'
  });

  const hanldeChangeInput = (key, value) => {
    setInput(prevInput => ({
      ...prevInput,
      [key]: value
    }));
  };

  const tabs = [
    {
      id: 'Display',
      content: 'Display',
      accessibilityLabel: 'Display',
      panelID: 'display'
    },
    {
      id: 'Triggers',
      content: 'Triggers',
      accessibilityLabel: 'Triggers',
      panelID: 'triggers'
    }
  ];

  const RangeSliderSettings = [
    {
      key: 'displayDuration',
      label: 'Display duration',
      helpText: 'How long each pop will display on your page.',
      max: 10,
      min: 1,
      step: 1,
      suffix: 'second(s)'
    },
    {
      key: 'firstDelay',
      label: 'Time before the first pop',
      helpText: 'The delay time before the first notification.',
      max: 10,
      min: 1,
      step: 1,
      suffix: 'second(s)'
    },
    {
      key: 'popsInterval',
      label: 'Gap time between two pops',
      helpText: 'The time interval between two popup notifications.',
      max: 10,
      min: 1,
      step: 1,
      suffix: 'second(s)'
    },
    {
      key: 'maxPopsDisplay',
      label: 'Maximum of popups',
      helpText:
        'The maximum number of popups are allowed to show after page loading. Maximum number is 80.',
      max: 80,
      min: 1,
      step: 1,
      suffix: 'pop(s)'
    }
  ];
  return (
    <div className="Avada-Setting__Wrapper">
      {loading && !input.shopId ? (
        <SettingEmpty />
      ) : (
        <Page
          title="Settings"
          subtitle="Decide how your notifications will play"
          fullWidth
          primaryAction={
            <Button loading={editing} onClick={() => handleEdit(input)}>
              Save
            </Button>
          }
        >
          <Layout sectioned>
            <Grid>
              <Grid.Cell columnSpan={{xs: 6, sm: 3, md: 3, lg: 3, xl: 3}}>
                <NotificationPopup {...input} />
              </Grid.Cell>
              <Grid.Cell columnSpan={{xs: 6, sm: 6, md: 6, lg: 12, xl: 9}}>
                <LegacyCard>
                  <LegacyTabs tabs={tabs} selected={selected} onSelect={handleTabChange}>
                    <LegacyCard.Section>
                      {selected === 0 ? (
                        <div className="Avada-Setting__Display-Wrapper">
                          <p className="Avada-Setting__Tab-Subtitle">APPEARANCE</p>
                          <DesktopPosition input={input} hanldeChangeInput={hanldeChangeInput} />
                          <div className="Avada-Setting__Checkbox">
                            <Checkbox
                              label="Hide time ago"
                              checked={input.hideTimeAgo}
                              onChange={value => hanldeChangeInput('hideTimeAgo', value)}
                            />
                          </div>
                          <div className="Avada-Setting__Checkbox">
                            <Checkbox
                              label="Truncate content text"
                              checked={input.truncateProductName}
                              onChange={value => hanldeChangeInput('truncateProductName', value)}
                              helpText="if your product name in long for one line, it will be truncated to 'Product na...'"
                            />
                          </div>
                          <p className="Avada-Setting__Tab-Subtitle">TIMING</p>
                          <Grid gap={10}>
                            {RangeSliderSettings.map(slider => (
                              <Grid.Cell
                                key={slider.key}
                                columnSpan={{xs: 6, sm: 6, md: 6, lg: 6, xl: 6}}
                              >
                                <RangeSlider
                                  output
                                  label={slider.label}
                                  value={input[slider.key]}
                                  step={slider.step}
                                  max={slider.max}
                                  min={slider.min}
                                  onChange={value => hanldeChangeInput(slider.key, value)}
                                  suffix={
                                    <TextField
                                      suffix={slider.suffix}
                                      readOnly
                                      value={input[slider.key]}
                                      autoComplete="off"
                                    />
                                  }
                                  helpText={slider.helpText}
                                />
                              </Grid.Cell>
                            ))}
                          </Grid>
                        </div>
                      ) : (
                        <Triggers input={input} hanldeChangeInput={hanldeChangeInput} />
                      )}
                    </LegacyCard.Section>
                  </LegacyTabs>
                </LegacyCard>
              </Grid.Cell>
            </Grid>
          </Layout>
        </Page>
      )}
    </div>
  );
}

Settings.propTypes = {};
