import React, {useCallback, useEffect, useMemo, useState} from 'react';
import {Autocomplete, TextField} from '@shopify/polaris';
import PropTypes from 'prop-types';

export default function Triggers(props) {
  const {input, hanldeChangeInput} = props;
  const deselectedOptions = useMemo(
    () => [
      {value: 'all', label: 'All pages'},
      {value: 'specific', label: 'Specific pages'}
    ],
    []
  );
  const [inputValue, setInputValue] = useState(deselectedOptions[0].label);
  const [options, setOptions] = useState(deselectedOptions);

  useEffect(() => {
    const option = deselectedOptions.find(el => el.value === input.allowShow);
    setInputValue(option.label);
  }, [input.allowShow]);

  const updateText = useCallback(
    value => {
      setInputValue(value);

      if (value === '') {
        setOptions(deselectedOptions);
        return;
      }

      const filterRegex = new RegExp(value, 'i');
      const resultOptions = deselectedOptions.filter(option => option.label.match(filterRegex));
      setOptions(resultOptions);
    },
    [deselectedOptions]
  );

  const updateSelection = useCallback(
    selected => {
      hanldeChangeInput('allowShow', selected[0]);
    },
    [options]
  );

  const textField = (
    <Autocomplete.TextField
      onChange={updateText}
      value={inputValue}
      placeholder="Select"
      autoComplete="off"
    />
  );

  return (
    <div style={{width: '100%'}}>
      <p className="Avada-Setting__Tab-Subtitle">PAGES RESTRICTION</p>
      <Autocomplete
        options={options}
        selected={[input.allowShow]}
        onSelect={updateSelection}
        textField={textField}
      />
      {input?.allowShow === 'specific' ? (
        <div style={{marginTop: '20px'}}>
          <TextField
            label="Included pages"
            value={input?.includedUrls || ''}
            onChange={value => hanldeChangeInput('includedUrls', value)}
            multiline={4}
            helpText="Page URLs to show the pop-up (separated by new lines)"
            autoComplete="off"
          />
        </div>
      ) : null}
      <div style={{marginTop: '20px'}}>
        <TextField
          label="Excluded pages"
          value={input?.excludedUrls || ''}
          onChange={value => hanldeChangeInput('excludedUrls', value)}
          multiline={4}
          helpText="Page URLs NOT to show the pop-up (separated by new lines)"
          autoComplete="off"
        />
      </div>
    </div>
  );
}
Triggers.propTypes = {
  input: PropTypes.object.isRequired,
  hanldeChangeInput: PropTypes.func.isRequired
};
