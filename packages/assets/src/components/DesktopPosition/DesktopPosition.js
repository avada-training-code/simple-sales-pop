import React from 'react';
import './DesktopPosition.scss';
import PropTypes from 'prop-types';

const DesktopPosition = props => {
  const {input, hanldeChangeInput} = props;
  const data = ['bottom-left', 'bottom-right', 'top-left', 'top-right'];
  const value = input.position;
  return (
    <div className="Avada-Desktop-Position__Wrapper">
      <p> Desktop Position</p>
      <div className="Avada-Desktop-Position__Container">
        {data?.map(el => (
          <div
            onClick={() => hanldeChangeInput('position', el)}
            key={el}
            className={
              el === value
                ? `Avada-Desktop-Position__Wrapper-Selected-Icon`
                : `Avada-Desktop-Position__Wrapper-Icon`
            }
          >
            <div
              className={
                el === value
                  ? `Avada-Desktop-Position__Selected-Icon Avada-Desktop-Position__${el}`
                  : `Avada-Desktop-Position__Icon Avada-Desktop-Position__${el}`
              }
            ></div>
          </div>
        ))}
      </div>
      <p className="Avada-Desktop-Position__Helper-Text">
        The display position of the pop on your website.
      </p>
    </div>
  );
};

DesktopPosition.propTypes = {
  input: PropTypes.object.isRequired,
  hanldeChangeInput: PropTypes.func.isRequired
};

export default DesktopPosition;
