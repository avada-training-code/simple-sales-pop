import {getShopByShopifyDomain} from '@avada/core';
import {createNotificationItem} from '@functions/services/notificationService';

/**
 *
 * @param ctx
 * @returns {Promise<{success: boolean}>}
 */
export async function listenNewOrder(ctx) {
  try {
    const shopifyDomain = ctx.get('X-Shopify-Shop-Domain');
    const orderData = ctx.req.body;
    const shopData = await getShopByShopifyDomain(shopifyDomain);
    await createNotificationItem({shopData, orderData});
    return (ctx.body = {success: true});
  } catch (error) {
    return (ctx.body = {
      success: false
    });
  }
}
