import {getNotificationsByShopifyDomain} from '@functions/repositories/notificationRepository';
import {getSettingByShopifyDomain} from '@functions/repositories/settingRepository';

/**
 *
 * @param ctx
 * @returns {Promise<{success: boolean}|{data: {notifications: Array | {id: *}, setting: Array | {id: *}}}>}
 */
export async function getNotifications(ctx) {
  try {
    const shopifyDomain = ctx.request.query.shopifyDomain;
    const [notifications, setting] = await Promise.all([
      getNotificationsByShopifyDomain(shopifyDomain),
      getSettingByShopifyDomain(shopifyDomain)
    ]);
    return (ctx.body = {
      data: {
        setting,
        notifications
      }
    });
  } catch (error) {
    ctx.status = 404;
    return (ctx.body = {
      success: false
    });
  }
}
