import {getCurrentUser} from '@functions/helpers/auth';
import {getNotificationsByShopId} from '@functions/repositories/notificationRepository';
/**
 * Get current subscription of a shop
 *
 * @param {Context|Object|*} ctx
 * @returns {Promise<{data: {}, success: boolean}>}
 */
export async function getNotifications(ctx) {
  try {
    const {shopID} = getCurrentUser(ctx);
    const notifications = await getNotificationsByShopId(shopID);

    return (ctx.body = {data: notifications, success: true});
  } catch (error) {
    console.log('error', error);
    ctx.status = 404;
    return (ctx.body = {
      data: {},
      success: false
    });
  }
}
