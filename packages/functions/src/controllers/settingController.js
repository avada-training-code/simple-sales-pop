import {getCurrentUser} from '@functions/helpers/auth';
import {getSettingByShopId, updateSetting} from '@functions/repositories/settingRepository';

/**
 * Get current subscription of a shop
 *
 * @param {Context|Object|*} ctx
 * @returns {Promise<{data: {}, success: boolean}>}
 */
export async function getSetting(ctx) {
  try {
    const {shopID} = getCurrentUser(ctx);
    const setting = await getSettingByShopId(shopID);
    return (ctx.body = {data: setting, success: true});
  } catch (error) {
    ctx.status = 404;
    return (ctx.body = {
      data: {},
      success: false
    });
  }
}

/**
 * Get current subscription of a shop
 *
 * @param {Context|Object|*} ctx
 * @returns {Promise<{success: boolean}>}
 */
export async function update(ctx) {
  try {
    const {shopID} = getCurrentUser(ctx);
    const resp = await updateSetting(shopID, ctx);
    return (ctx.body = resp);
  } catch (error) {
    ctx.status = 404;
    return (ctx.body = {
      data: {},
      success: false
    });
  }
}
