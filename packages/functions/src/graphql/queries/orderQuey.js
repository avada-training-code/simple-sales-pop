/**
 *
 * @type {string}
 */
export const getOrdersQuery = `query getOrders($limit: Int!){
  orders(first: $limit) {
    edges {
      node {
        createdAt
        customer {
          firstName
          defaultAddress {
            city
            country
          }
        }
        lineItems(first: 1) {
          edges {
            node {
              image {
                url
              }
              product {
                id
                title
              }
            }
          }
        }
      }
    }
  }
}`;

/**
 *
 * @type {string}
 */
export const getOrderQueryById = `query($id: ID!){
  order(id: $id) {
    createdAt
    customer {
      defaultAddress {
        city
        country
      }
      firstName
    }
    lineItems(first:1) {
      nodes {
        id
        image {
          url
        }
        name
      }
    }
  }
}`;
