import App from 'koa';
import * as errorService from '@functions/services/errorService';
import apiRouter from '@functions/routes/webhook';

// Initialize all demand configuration for an application
const api = new App();
api.proxy = true;
// Register all routes for the application
api.use(apiRouter.allowedMethods());
api.use(apiRouter.routes());

// Handling all errors
api.on('error', errorService.handleError);

export default api;
