const defaultSetting = {
  allowShow: 'all',
  displayDuration: 1,
  excludedUrls: '',
  firstDelay: 1,
  hideTimeAgo: false,
  includedUrls: '',
  maxPopsDisplay: 1,
  popsInterval: 1,
  position: 'bottom-left',
  truncateProductName: true,
  shopId: '',
  shopifyDomain: ''
};

export default defaultSetting;
