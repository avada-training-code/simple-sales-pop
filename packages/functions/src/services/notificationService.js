import {getShopByShopifyDomain} from '@avada/core';
import Shopify from 'shopify-api-node';
import {createNotification} from '@functions/repositories/notificationRepository';
import {getOrderQueryById, getOrdersQuery} from '@functions/graphql/queries/orderQuey';

/**
 *
 * @param shopifyDomain
 * @param limit
 * @returns {Promise<void>}
 */
export async function syncNotifications(shopifyDomain, limit = 30) {
  const shopData = await getShopByShopifyDomain(shopifyDomain);
  const shopify = new Shopify({
    accessToken: shopData.accessToken,
    shopName: shopData.shopifyDomain
  });
  await createNotificationsByOrders({shopData, shopify, limit});
}

/**
 *
 * @param shopData
 * @param orderData
 * @returns {Promise<void>}
 */
export async function createNotificationItem({shopData, orderData}) {
  const shopify = new Shopify({
    accessToken: shopData.accessToken,
    shopName: shopData.shopifyDomain
  });
  await createNotificationItemByOrder({shopData, shopify, orderData});
}

/**
 * @param {Object} shopData
 * @param {Object} shopify
 * @param {Object} order
 * @returns {Object}
 */
export async function getNotificationsItemByOrders({shopData, shopify, limit}) {
  const data = await shopify.graphql(getOrdersQuery, {limit});
  const newNotification = data.orders.edges?.map(({node}) => ({
    city: node.customer.defaultAddress.city,
    country: node.customer.defaultAddress.country,
    firstName: node?.customer.firstName,
    productId: node.lineItems.edges[0].node.product.id,
    productImage: node.lineItems.edges[0].node.image?.url,
    productName: node.lineItems.edges[0].node.product.title,
    timestamp: node?.createdAt,
    shopId: shopData.id,
    shopifyDomain: shopData.shopifyDomain
  }));
  return newNotification;
}

/**
 *
 * @param shopData
 * @param shopify
 * @param orderData
 * @returns {Promise<{country: *, firstName: *, productImage: *, productId: number, city: *, shopId, shopifyDomain, productName, timestamp: *}>}
 */
export async function createNotificationItemByOrder({shopData, shopify, orderData}) {
  const id = orderData.admin_graphql_api_id;
  const data = await shopify.graphql(getOrderQueryById, {id});
  const newNotification = {
    city: data.order.customer.defaultAddress?.city,
    country: data.order.customer.defaultAddress?.country,
    firstName: data.order.customer.firstName,
    productId: data.order.lineItems.nodes[0].id,
    productImage: data?.order?.lineItems?.nodes[0]?.image?.url || null,
    productName: data.order.lineItems.nodes[0].name,
    timestamp: data.order.createdAt,
    shopId: shopData.id,
    shopifyDomain: shopData.shopifyDomain
  };
  await createNotification(newNotification);
}

/**
 *
 * @param shopData
 * @param shopify
 * @param limit
 * @returns {Promise<void>}
 */
export async function createNotificationsByOrders({shopData, shopify, limit}) {
  const notifications = await getNotificationsItemByOrders({shopData, shopify, limit});
  if (notifications.length > 0) {
    notifications.forEach(doc => {
      createNotification(doc);
    });
  }
}
