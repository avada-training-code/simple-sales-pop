import {Firestore} from '@google-cloud/firestore';

/**
 * @documentation
 *
 * Only use one repository to connect to one collection
 * do not connect more than one collection from one repository
 */
const firestore = new Firestore();
/** @type CollectionReference */
const collection = firestore.collection('notifications');

/**
 *
 * @param shopId
 * @returns {Promise<(*&{id: *})[]|null>}
 */
export async function getNotificationsByShopId(shopId) {
  const notificationDocs = await collection.where('shopId', '==', shopId).get();
  if (notificationDocs.empty) {
    return null;
  }
  return notificationDocs.docs.map(doc => ({
    id: doc.id,
    ...doc.data()
  }));
}

/**
 *
 * @param shopifyDomain
 * @returns {Promise<(*&{id: *})[]|null>}
 */
export async function getNotificationsByShopifyDomain(shopifyDomain) {
  const notificationDocs = await collection.where('shopifyDomain', '==', shopifyDomain).get();
  if (notificationDocs.empty) {
    return null;
  }
  return notificationDocs.docs.map(doc => ({
    id: doc.id,
    ...doc.data()
  }));
}

/**
 *
 * @param data
 * @returns {Promise<void>}
 */
export async function createNotification(data) {
  await collection.add(data);
}
