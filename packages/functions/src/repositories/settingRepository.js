import {getShopByShopifyDomain} from '@avada/core';
import defaultSetting from '@functions/const/settings/defaultSettings';
import {Firestore} from '@google-cloud/firestore';

/**
 * @documentation
 *
 * Only use one repository to connect to one collection
 * do not connect more than one collection from one repository
 */
const firestore = new Firestore();
/** @type CollectionReference */
const collection = firestore.collection('settings');

/**
 *
 * @param shopId
 * @returns {Promise<unknown>}
 */
export async function getSettingByShopId(shopId) {
  const settingDocs = await collection
    .where('shopId', '==', shopId)
    .limit(1)
    .get();
  if (settingDocs.empty) {
    return null;
  }
  const settings = settingDocs.docs?.map(doc => ({
    id: doc.id,
    ...doc.data()
  }));
  return {
    ...settings[0]
  };
}

/**
 *
 * @param shopifyDomain
 * @returns {Promise<unknown>}
 */
export async function getSettingByShopifyDomain(shopifyDomain) {
  const settingDocs = await collection
    .where('shopifyDomain', '==', shopifyDomain)
    .limit(1)
    .get();
  if (settingDocs.empty) {
    return null;
  }
  const settings = settingDocs.docs?.map(doc => ({
    id: doc.id,
    ...doc.data()
  }));
  return {
    ...settings[0]
  };
}

/**
 *
 * @param shopId
 * @param ctx
 * @returns {Promise<{success: boolean}>}
 */
export async function updateSetting(shopId, ctx) {
  const postData = ctx.req?.body;
  const settingDocs = await collection
    .where('shopId', '==', shopId)
    .limit(1)
    .get();
  if (settingDocs.empty) {
    return;
  }
  settingDocs.docs.map(doc => collection.doc(doc.id).update(postData.data));

  return {
    success: true
  };
}

/**
 *
 * @param shopifyDomain
 * @returns {Promise<{success: boolean}>}
 */
export async function createDefaultSetting(shopifyDomain) {
  const shopData = await getShopByShopifyDomain(shopifyDomain);
  const data = {
    ...defaultSetting,
    shopId: shopData.id,
    shopifyDomain: shopifyDomain
  };
  const settingDocs = await collection
    .where('shopifyDomain', '==', shopifyDomain)
    .limit(1)
    .get();
  if (settingDocs.empty) {
    await collection.add(data);
    return {
      success: true
    };
  }
  settingDocs.docs.map(doc => {
    collection.doc(doc.id).update(data);
  });
  return {
    success: true
  };
}
