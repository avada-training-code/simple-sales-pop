import React from 'react';
import './NoticationPopup.scss';
import {Icon} from '@shopify/polaris';
import {CheckIcon} from '@shopify/polaris-icons';
import PropTypes from 'prop-types';

const NotificationPopup = ({
  firstName = 'John Doe',
  city = 'New York',
  country = 'United States',
  productName = 'Puffer Jacket',
  timestamp = 'a day ago',
  productImage = 'https://kingshoes.vn/data/upload/media/giay-nike-air-force-1-nam-chinh-hang-tai-tphcm-AJ7747-100-king-shoes-sneaker-authentic-2.jpg',
  position = 'bottom-left',
  hideTimeAgo = false,
  truncateProductName = false,
  includedUrls = '',
  excludedUrls = '',
  allowShow = 'all'
}) => {
  const truncateString = ({str, truncateProductName = true, maxLength = 10}) => {
    if (str.length > maxLength && truncateProductName) {
      return str.slice(0, maxLength) + '...';
    }
    return str;
  };
  const productNameData = truncateString({str: productName, truncateProductName, maxLength: 10});
  return (
    <div
      className={`Avada-SP__Wrapper Avada-SP__Wrapper-${position} fadeInUp animated Avada-SP__Text`}
    >
      <div className="Avada-SP__Inner">
        <div className="Avada-SP__Container">
          <a href="#" className={'Avada-SP__LinkWrapper'}>
            <div
              className="Avada-SP__Image"
              style={{
                backgroundImage: `url(${productImage})`
              }}
            ></div>
            <div className="Avada-SP__Content">
              <div className={'Avada-SP__Title'}>
                {firstName} in {city}, {country}
              </div>
              <div className={'Avada-SP__Subtitle'}>purchased {productNameData}</div>
              <div className={'Avada-SP__Footer'}>
                {!hideTimeAgo && <div>{timestamp} </div>}
                <div style={{display: 'flex'}}>
                  <span className="uni-blue">
                    <Icon source={CheckIcon} />
                  </span>
                  <span className="uni-blue">by AVADA</span>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  );
};

NotificationPopup.propTypes = {
  firstName: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired,
  productName: PropTypes.string.isRequired,
  timestamp: PropTypes.string.isRequired,
  productImage: PropTypes.string.isRequired,
  position: PropTypes.string.isRequired,
  hideTimeAgo: PropTypes.bool.isRequired,
  truncateProductName: PropTypes.bool.isRequired,
  includedUrls: PropTypes.string,
  excludedUrls: PropTypes.string,
  allowShow: PropTypes.string
};

export default NotificationPopup;
