import {insertAfter} from '../helpers/insertHelpers';
import {render} from 'preact';
import React from 'preact/compat';
import NotificationPopup from '../components/NotificationPopup/NotificationPopup';

export default class DisplayManager {
  constructor() {
    this.notifications = [];
    this.settings = {};
  }
  async initialize({notifications, setting}) {
    this.notifications = notifications;
    this.settings = setting;
    this.insertContainer();
    const delay = ms => new Promise(res => setTimeout(res, ms * 1000));
    const showFirstTime = async () => {
      await delay(setting.firstDelay);
    };

    await showFirstTime();
    for (const notification of notifications) {
      await this.display({notification, setting});
      await delay(setting.displayDuration);
      await this.fadeOut();
      await delay(setting.popsInterval);
    }
  }

  fadeOut() {
    const container = document.querySelector('#Avada-SalePop');
    container.innerHTML = '';
  }

  display({notification, setting}) {
    const container = document.querySelector('#Avada-SalePop');
    console.log('container', container);
    render(<NotificationPopup {...notification} {...setting} />, container);
  }

  insertContainer() {
    const popupEl = document.createElement('div');
    popupEl.id = `Avada-SalePop`;
    popupEl.classList.add('Avada-SalePop__OuterWrapper');
    const targetEl = document.querySelector('body').firstChild;
    if (targetEl) {
      insertAfter(popupEl, targetEl);
    }

    return popupEl;
  }
}
