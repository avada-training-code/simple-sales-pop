import makeRequest from '../helpers/api/makeRequest';

export default class ApiManager {
  getNotifications = async () => {
    return this.getApiData();
  };

  getApiData = async () => {
    const shopifyDomain = window.Shopify.shop;
    const data = await makeRequest(
      `https://riders-equipped-surgeon-resolutions.trycloudflare.com/clientApi/notifications?shopifyDomain=${shopifyDomain}`
    );
    const {notifications, setting} = data.data;
    return {notifications, setting};
  };
}
